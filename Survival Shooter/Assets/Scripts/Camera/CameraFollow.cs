﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;       // The position that camera will be following.
    public float smoothing = 5f;   // The speed with which the camera will be following.

    Vector3 offest;                // The initial offset from the target.

    void Start()
    {
        // Calculate the initial offset.
        offest = transform.position - target.position;

    }

    void FixedUpdate()
    {
        // Create a position the camera is aiming for based on the offest from the target.
        Vector3 targetCamPos = target.position + offest;

        // Smoothly interpolate between the camera's current position and it's targer position.
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);

    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;        // The Speed that the player will move at.
    public Text invincibleText;
    public Text lazerText;
    Vector3 movement;               // The vector to store the direction of the player's movement.
    Animator anim;                  // Reference to the animator component.
    Rigidbody playerRigidbody;      // Reference to the player's rigidbody.
    int floorMask;                  // A layer mask so that a ray can be cast just as gameobjects on the floor layer.
    float camRayLength = 100f;      // The length of the ray from the camera into the scene.

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("lazer"))                   //if player hits the lazer pickup
        {
            other.gameObject.GetComponent<AudioSource>().Play();
            lazerText.color = new Color(lazerText.color.r, lazerText.color.g, lazerText.color.b, 1);
            PlayerShooting.damagePerShot = 20;
            PlayerShooting.timeBetweenBullets = .01f;
            Destroy(other.gameObject);
            StopCoroutine(WeaponReset());
            StartCoroutine(WeaponReset());
        }

        if (other.gameObject.CompareTag("speed"))                   //if player hits the speed pickup
        {
            other.gameObject.GetComponent<AudioSource>().Play();
            invincibleText.color = new Color(invincibleText.color.r, invincibleText.color.g, invincibleText.color.b, 1);
            speed = 12f;
            PlayerHealth.invincible = true;
            Destroy(other.gameObject);
            StopCoroutine(SpeedReset());
            StartCoroutine(SpeedReset());

        }
    }
    IEnumerator WeaponReset ()                                      //resets players weapon back to default

    {
        yield return new WaitForSeconds(15);
        lazerText.color = new Color(lazerText.color.r, lazerText.color.g, lazerText.color.b, 0);
        PlayerShooting.damagePerShot = 20;
        PlayerShooting.timeBetweenBullets = .15f;
        
    }

    IEnumerator SpeedReset ()                                       //resets players speed back to default

    {
        yield return new WaitForSeconds(7.5f);
        invincibleText.color = new Color(invincibleText.color.r, invincibleText.color.g, invincibleText.color.b, 0);
        speed = 6f;
        PlayerHealth.invincible = false;


    }

    void Awake()
    {
        // Create a layer mask for the floor layer.
        floorMask = LayerMask.GetMask("Floor");

        // Set up references.
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();

    }

     void FixedUpdate()
    {
        // Store the input axes.
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // Move the player around the scene.
        Move(h, v);

        //Turn the player to face the mouse cursor.
        Turning();

        // Animate the player.
        Animating(h, v);

    }

    void Move (float h, float v)
    {
        // Set the movement vector based on the axis input.
        movement.Set(h, 0f, v);

        // Normallise the movemement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        playerRigidbody.MovePosition(transform.position + movement);

    }
    
    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

        // Perform the raycst if it hits something on the floor layer...
        if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely aloong the floor plane.
            playerToMouse.y = 0f;

            // Create a Quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating (float h, float v)
    {
        // Create a boolean that is true if eiother of the input axes is non zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", walking);

    }
}

﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
    public GameObject lazerPickup;
    public GameObject speedIncrease;
    public GameObject healthPickup;

    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    bool isDead;
    bool isSinking;
    float percentDropForLazer = 3f;
    float percentDropForSpeed = 5f;
    float percentDropForHealth = 2f;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();  
        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if(isDead)
            return;

        enemyAudio.Play ();

        currentHealth -= amount;   
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();

        float randWeapon = Random.Range(0f, 100f);
        float randSpeed = Random.Range(0f, 100f);
        float randHealth = Random.Range(0f, 100f);

        if (randWeapon < percentDropForLazer)
        {
            Instantiate(lazerPickup, transform.position, transform.rotation);
        }

        if (randSpeed < percentDropForSpeed)
        {
            Instantiate(speedIncrease, transform.position, transform.rotation);
        }

        if (randHealth < percentDropForHealth)
        {
            Instantiate(healthPickup, transform.position, transform.rotation);
        }
    }



    public void StartSinking ()
    {
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue;
        Destroy (gameObject, 2f);
    }
}
